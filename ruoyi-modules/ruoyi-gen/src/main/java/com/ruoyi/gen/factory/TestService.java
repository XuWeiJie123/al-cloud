package com.ruoyi.gen.factory;


public interface TestService {

    String firstStep();

    String secondStep();

    String thirdStep();
}
