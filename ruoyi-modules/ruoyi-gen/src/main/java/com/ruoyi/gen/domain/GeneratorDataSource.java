package com.ruoyi.gen.domain;

import com.ruoyi.common.core.annotation.Excel;
import com.ruoyi.common.security.entity.BaseZrdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 数据源对象 generator_data_source
 *
 * @author ruoyi
 * @date 2020-12-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GeneratorDataSource extends BaseZrdEntity implements Serializable {

    private static final long serialVersionUID = 9194850255634640793L;
    /**
     * 主键
     */
    private Long id;

    /**
     * 索引关键字
     */
    @Excel(name = "索引关键字" )
    private String dbKey;

    /**
     * 描述
     */
    @Excel(name = "描述" )
    private String description;

    /**
     * 驱动
     */
    @Excel(name = "驱动" )
    private String driverClass;

    /**
     * URL
     */
    @Excel(name = "URL" )
    private String url;

    /**
     * 帐号
     */
    @Excel(name = "帐号" )
    private String dbUser;

    /**
     * 密码
     */
    @Excel(name = "密码" )
    private String dbPassword;

    /**
     * 数据库类型
     */
    @Excel(name = "数据库类型" )
    private String dbType;

    /**
     * 数据库名称
     */
    @Excel(name = "数据库名称" )
    private String dbName;

    /**
     * remarks
     */
    @Excel(name = "remarks" )
    private String remarks;

    /**
     * del_flag
     */
    private String delFlag;



}
