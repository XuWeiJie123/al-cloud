package com.ruoyi.gen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.gen.domain.GeneratorDataSource;

import java.util.List;

/**
 * 数据源Service接口
 *
 * @author ruoyi
 * @date 2020-12-24
 */
public interface IGeneratorDataSourceService extends IService<GeneratorDataSource> {


    /**
     * 查询数据源列表
     *
     * @param generatorDataSource 数据源
     * @return 数据源集合
     */
    List<GeneratorDataSource> selectGeneratorDataSourceList(GeneratorDataSource generatorDataSource);


}
